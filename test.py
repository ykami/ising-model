#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Yoshinori Kamijima'
__date__ = '2020/6/21'

import math
import numpy as np
from numpy.lib.arraysetops import isin

#import simulator
import simulatorWithCpp as simulator

MaxNodes = 16

# 2-dimensional square lattice.
def GenerateSquareLatticeEdges(maxNodes: int) -> dict[tuple[int, int], float]:
    columns = math.ceil(math.sqrt(maxNodes))
    result = {}
    for i in range(maxNodes - 1):
        if (i + 1) % columns > 0:
            result[(i, i + 1)] = -1
        if (i + columns) < maxNodes:
            result[(i, i + columns)] = -1
    return result

if __name__ == '__main__':
    quadratic = GenerateSquareLatticeEdges(MaxNodes)
    isingModel = simulator.IsingModel({}, quadratic)
    isingModel.Spins = simulator.MakeUniformConfiguration(set(isingModel.Spins.keys()))  # Pythonのみならsetは不要だが，C++とのコードを共通化するために変換．
    isingModel.Temperature = len(quadratic)
    isingModel.Write()
    for i in range(MaxNodes**2):
        isingModel.Update()
    print(isingModel.Spins)
