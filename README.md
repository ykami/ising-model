# An Ising model simulator

## Build

```console
> python3 -m venv venv
> venv/Script/activate
> set VCPKG_ROOT=/path/to/vcpkg
> pip install .
```