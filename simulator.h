﻿#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <Eigen/Core>
#include <algorithm>
#include <cmath>
#include <map>
#include <memory>
#include <optional>
#include <random>
#include <set>
#include <stdexcept>
#include <string>
#include <utility>
#include <variant>
#include <vector>

// Ref: https://qiita.com/Gaccho/items/dc312fb5a056505f0a9f
class Rand {
public:
	Rand()
	{
		mt.seed(rd());
	}

	void Seed()
	{
		mt.seed(rd());
	}

	void Seed(const std::int_fast64_t seed)
	{
		mt.seed(seed);
	}

	std::uint_fast64_t operator()()
	{
		return mt();
	}

	std::int_fast64_t operator()(const std::int_fast64_t maximum)
	{
		std::uniform_int_distribution<> distr(0, (maximum >= 0) ? maximum - 1 : 0);
		return distr(mt);
	}

	std::int_fast64_t operator()(const std::int_fast64_t minimum, const std::int_fast64_t maximum)
	{
		std::uniform_int_distribution<> distr((minimum <= maximum) ? minimum : maximum, (minimum <= maximum) ? maximum : minimum);
		return distr(mt);
	}

	bool Bernoulli(const double probability)
	{
		std::bernoulli_distribution distr(probability);
		return distr(mt);
	}

	double Exponential(const double intensity = 1.e0)
	{
		std::exponential_distribution<double> distr(intensity);
		return distr(mt);
	}

	double Logistic(const double location = 0.e0, const double scale = 1.e0)
	{
		double u = Uniform();
		return location + scale * std::log(u / (1.e0 - u));  // 逆関数法による生成。
	}

	double Uniform()
	{
		std::uniform_real_distribution<double> distr(0.e0, 1.e0);
		return distr(mt);
	}

	template<typename T>
	std::vector<T> Choice(const std::vector<T> population, const std::uint_fast64_t distance)
	{
		std::vector<T> result;
		std::sample(population.begin(), population.end(), std::back_inserter(result), distance, mt);
		return result;
	}

	template<typename T>
	T Choice(const std::vector<T> population)
	{
		return Choice(population, 1)[0];
	}
private:
	std::random_device rd;
	std::mt19937_64 mt;
};

namespace Simulator {
	using Node = std::variant<int, std::string>;
	using Edge = std::pair<Node, Node>;
	using LinearBiases = std::map<Node, double>;
	using QuadraticBiases = std::map<Edge, double>;
	using NodeKeys = std::set<Node>;

	enum class Algorithms {
		Metropolis,
		Glauber,
		SCA,
		fcSCA,
		MA,
		MMA,
		HillClimbing,
		SIZE
	};

	class IsingModel {
	public:
		enum class Spin : int {  // ライブラリ側でも型変換できるように、enum classではなくenumを使う。
			Down = -1,
			Up = +1
		};

		IsingModel(const LinearBiases linear, const QuadraticBiases quadratic);
		double CalcLargestEigenvalue() const;
		double GetEnergy() const;
		double GetEnergyOnBipartiteGraph() const;
		void Update();
		void Write() const;

		// 外部磁場の強さを取得．
		double GetBias(const Node node) const
		{
			return externalMagneticField(nodeIndices.at(node));
		}

		void SetBias(const Node node, const double bias)
		{
			if (auto itr = nodeIndices.find(node); itr != nodeIndices.end()) {
				externalMagneticField[nodeIndices.at(node)] = bias;
			} else {
				std::string temp;
				if (std::holds_alternative<int>(node))
					temp = std::to_string(std::get<int>(node));
				else
					temp = std::get<std::string>(node);
				throw std::out_of_range(temp + " is not found.");
			}
		}

		// 結合係数を取得．
		double GetBias(const Edge edge) const
		{
			return couplingCoefficients(nodeIndices.at(edge.first), nodeIndices.at(edge.second));
		}

		void SetBias(const Edge edge, const double bias)
		{
			auto itr1 = nodeIndices.find(edge.first);
			auto itr2 = nodeIndices.find(edge.second);
			if (itr1 != nodeIndices.end() && itr2 != nodeIndices.end()) {
				couplingCoefficients(nodeIndices.at(edge.first), nodeIndices.at(edge.second))
				= couplingCoefficients(nodeIndices.at(edge.second), nodeIndices.at(edge.first))
				= bias;
			} else {
				std::string temp = "(";
				if (std::holds_alternative<int>(edge.first))
					temp += std::to_string(std::get<int>(edge.first));
				else
					temp += std::get<std::string>(edge.first);
				temp += ", ";
				if (std::holds_alternative<int>(edge.second))
					temp += std::to_string(std::get<int>(edge.second));
				else
					temp += std::get<std::string>(edge.second);
				temp += ")";
				throw std::out_of_range(temp + " is not found.");
			}
		}

		void SetSeed()
		{
			rand = std::make_unique<Rand>();
		}

		void SetSeed(const unsigned int seed)
		{
			SetSeed();
			rand->Seed(seed);
		}

		Algorithms GetCurrentAlgorithm() const
		{
			return algorithm;
		}

		void ChangeAlgorithmTo(const Algorithms algorithm)
		{
			this->algorithm = algorithm;
		}

		double GetTemperature() const
		{
			return temperature;
		}

		void SetTemperature(const double temperature)
		{
			this->temperature = std::max(temperature, 0.e0);
		}

		double GetPinningParameter() const
		{
			return pinningParameter;
		}

		void SetPinningParameter(const double pinningParameter)
		{
			this->pinningParameter = std::max(pinningParameter, 0.e0);
		}

		double GetFlipTrialRate() const
		{
			return flipTrialRate;
		}

		void SetFlipTrialRate(const double flipTrialRate)
		{
			this->flipTrialRate = std::min(std::max(flipTrialRate, 0.e0), 1.e0);
		}

		std::map<Node, Spin> GetSpinsAsDictionary() const
		{
			std::map<Node, Spin> result;
			for (const auto& node : nodeIndices)
				result[node.first] = spins[node.second];
			return result;
		}

		void SetSpinsAsDictionary(const std::map<Node, Spin> spins)
		{
			for (const auto& spin : spins)
				this->spins[nodeIndices[spin.first]] = spin.second;
		}

		Eigen::VectorXi GetSpins() const
		{
			return spins.cast<int>();
		}

		Eigen::VectorXd GetExternalMagneticField() const
		{
			return externalMagneticField;
		}

		Eigen::MatrixXd GetCouplingCoefficients() const
		{
			return couplingCoefficients;
		}
	private:
		using Configuration = Eigen::Matrix<Spin, Eigen::Dynamic, 1>;

		std::unique_ptr<Rand> rand;
		double temperature;        // Including the Boltzmann constant: k_B T.
		double pinningParameter;   // Pinning parameter of SCA.
		double flipTrialRate;      // Flip trial rate of flip-constained SCA.
		Algorithms algorithm;
		std::map<Node, std::size_t> nodeIndices;
		Configuration spins;
		Configuration previousSpins;
		Eigen::VectorXd externalMagneticField;
		Eigen::MatrixXd couplingCoefficients;

		double calcLocalMagneticField(const unsigned int nodeIndex) const
		{
			return (couplingCoefficients * spins.cast<double>())(nodeIndex) + externalMagneticField(nodeIndex);
		}

		Eigen::VectorXd calcLocalMagneticField(const Configuration& spins) const
		{
			return couplingCoefficients * spins.cast<double>() + externalMagneticField;
		}

		Spin flip(const Spin spin) const
		{
			return (spin == Spin::Down) ? Spin::Up : Spin::Down;
		}
	};

	std::string AlgorithmToStr(const Algorithms algorithm);
	std::map<Node, IsingModel::Spin> MakeUniformConfiguration(const NodeKeys nodeKeys, const std::optional<unsigned int> seed = std::nullopt);
}

#endif // !SIMULATOR_H
