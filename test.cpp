﻿#include "simulator.h"
#include <cmath>
#include <iostream>
#include <numeric>

Simulator::QuadraticBiases generateSquareLatticeEdges(const int MaxNodes)
{
    auto columns = static_cast<int>(std::ceil(std::sqrt(MaxNodes)));
    Simulator::QuadraticBiases result;
    for (auto i = 0; i < MaxNodes; i++) {
        if ((i + 1) % columns > 0)
            result[{i, i + 1}] = -1.e0;
        else
            result[{i, i + columns}] = -1.e0;
    }
    return result;
}

int main()
{
    const unsigned int MaxNodes = 16;
    auto quadratic = generateSquareLatticeEdges(MaxNodes);
    Simulator::IsingModel isingModel({}, quadratic);

    // Ref: https://stackoverflow.com/questions/110157/how-to-retrieve-all-keys-or-values-from-a-stdmap-and-put-them-into-a-vector
    std::set<Simulator::Node> keys;
    for (const auto& [key, _] : isingModel.GetSpinsAsDictionary())
        keys.insert(key);
    isingModel.SetSpinsAsDictionary(Simulator::MakeUniformConfiguration(keys));

    isingModel.SetTemperature(std::accumulate(
        quadratic.begin(), quadratic.end(), 0.e0,
        [](double acc, const Simulator::QuadraticBiases::value_type& p) -> double { return acc + std::abs(p.second); }
    ));

    isingModel.Write();
    for (auto n = 0; n <= std::pow(MaxNodes, 2); n++)
        isingModel.Update();
    std::cout << isingModel.GetSpins().transpose() << std::endl;
    return 0;
}
